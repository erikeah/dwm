/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 10;        /* gaps between windows */
static const unsigned int snap      = 0;       /* snap pixel */
static const int centerlonely       = 1;        /* 0 means no center lonely window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char font[]	    = "monospace:style=Bold:size=10";
static const char emoji_font[]	    = "emoji:style=Regular:size=10";
static const char symbol_font[]	    = "symbol:style=Regular:size=10";
static const char *fonts[]          = { 
	font,
	emoji_font,
	symbol_font,
};
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#1c1c1c";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#999999";
static const char col_gray4[]       = "#bbbbbb";
static const char col_gray5[]       = "#eeeeee";
static const char col_accent[]      = "#2c78bf";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray5, col_accent,  col_accent  },
	[SchemeStatus]  = { col_gray4, col_gray1,  "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_gray5, col_accent,  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
	[SchemeTagsNorm]  = { col_gray3, col_gray1,  "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
	[SchemeInfoSel]  = { col_gray5, col_gray1,  "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm]  = { col_gray3, col_gray1,  "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

static const unsigned int almostopaque = 0xf0;
static const unsigned int almosttransparent = 0xa0;
static const unsigned int borderalpha = OPAQUE;
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, almostopaque, borderalpha },
	[SchemeSel]  = { OPAQUE, almostopaque, borderalpha },
	[SchemeStatus]  = { OPAQUE, almosttransparent, borderalpha }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { OPAQUE, almostopaque, borderalpha }, // Tagbar left selected {text,background,not used but cannot be empty}
	[SchemeTagsNorm]  = { OPAQUE, almosttransparent, borderalpha }, // Tagbar left unselected {text,background,not used but cannot be empty}
	[SchemeInfoSel]  = { OPAQUE, almosttransparent, borderalpha }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm]  = { OPAQUE, almosttransparent, borderalpha }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "7", "8", "9",};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      		instance    		title       tags mask     isfloating   monitor */
	{ "Gimp",     		NULL,       		NULL,       0,            1,           -1 },
	{ "Pinentry-gtk-2",     "pinentry-gtk-2",       NULL,       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_accent, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *print[]  = { "maimpick", NULL };
static const char *filemanager[]  = { "st", "-T", "lf", "-e", "lf", NULL };
static const char *browser[] = { "firefox", NULL };
static const char *lock[] = { "dm-tool", "lock", NULL };
static const char *esound[3][3]  = { 
	{"esound", "mute", NULL},
	{"esound", "down", NULL},
	{"esound", "up", NULL},
};
static const char *backlight[2][4] = {
	{"xbacklight", "-dec", "1", NULL},
	{"xbacklight", "-inc", "1", NULL},
};
static const char *audiocontrol[3][3] = {
	{"playerctl", "play-pause", NULL},
	{"playerctl", "previous", NULL},
	{"playerctl", "next", NULL},
};

static Key keys[] = {
	/* modifier                     key        			function        argument */
	{ MODKEY,                       XK_p,      			spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, 			spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      			togglebar,      {0} },
	{ MODKEY,                       XK_c,      			togglecenterlonely,      {0} },
	{ MODKEY,                       XK_j,      			focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      			focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      			incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      			incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      			setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      			setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, 			zoom,           {0} },
	{ MODKEY,                       XK_Tab,    			view,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      			killclient,     {0} },
	{ MODKEY,                       XK_t,      			setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      			setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      			setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  			setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  			togglefloating, {0} },
	{ MODKEY,                       XK_0,      			view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      			tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  			focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, 			focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,            	XK_comma,  			tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,            	XK_period, 			tagmon,         {.i = +1 } },
	{ 0,				XF86XK_AudioPlay,		spawn,          {.v = &audiocontrol[0] } },
	{ 0,				XF86XK_AudioPrev,		spawn,          {.v = &audiocontrol[1] } },
	{ 0,				XF86XK_AudioNext,		spawn,          {.v = &audiocontrol[2] } },
	{ 0,				XF86XK_AudioMute,		spawn,          {.v = &esound[0] } },
	{ 0,				XF86XK_AudioLowerVolume, 	spawn,          {.v = &esound[1] } },
	{ 0,				XF86XK_AudioRaiseVolume, 	spawn,          {.v = &esound[2] } },
	{ 0,				XF86XK_MonBrightnessDown,	spawn,          {.v = &backlight[0] } },
	{ 0,				XF86XK_MonBrightnessUp,  	spawn,          {.v = &backlight[1] } },
	{ MODKEY,		  	XK_o, 	   			spawn,          {.v = &filemanager } },
	{ MODKEY,		  	XK_w, 	   			spawn,          {.v = &browser } },
	{ MODKEY|ShiftMask,		XK_Delete,			spawn,		{.v = &lock  } },
	{ 0,				XK_Print,			spawn,          {.v = &print } },
	{ MODKEY,                       XK_minus,  			setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_plus,			setgaps,        {.i = +1 } },
	{ MODKEY,			XK_ccedilla,			setgaps,	{.i = 0  } },
	TAGKEYS(                        XK_1,      			                0)
	TAGKEYS(                        XK_2,      			                1)
	TAGKEYS(                        XK_3,      			                2)
	TAGKEYS(                        XK_4,      			                3)
	TAGKEYS(                        XK_6,      			                2)
	TAGKEYS(                        XK_7,      			                3)
	TAGKEYS(                        XK_8,      			                4)
	TAGKEYS(                        XK_9,      			                5)
	{ MODKEY|ShiftMask,             XK_c,      			quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

